package stack;
public class StackItem {
    Object item;
    StackItem next;
    StackItem(Object item) {
        this.item = item;
    }
    void setNext(StackItem next)
    {
        this.next = next;
    }
    Object getItem() {
        return item;
    }
    StackItem getNext()
    {
        return next;
    }
}

