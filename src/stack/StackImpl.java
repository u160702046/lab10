package stack;
public class StackImpl {
    StackItem top = null;
    public StackImpl(){
    }
    public boolean empty() {
        return top == null; }
    public void push(Object item) {
        StackItem stem;
        stem = new StackItem(item);
        stem.setNext(null);
        if (empty()) {
        } else {
            stem.setNext(top);
        }
        top = stem;
    }
    public Object pop() {
        if (empty()) {
            return null;
        }
        Object item = top.getItem();
        top = top.getNext();
        return item;
    }
}
