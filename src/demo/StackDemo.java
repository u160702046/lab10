package demo;
import stack.StackImpl;
public class StackDemo {
    public static void main(String[] args){
        StackImpl stack = new StackImpl();
        stack.push('A');
        stack.push('B');
        stack.push('C');
        stack.push('D');
        while(!stack.empty()) System.out.println(stack.pop());
    }
}

